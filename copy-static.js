const path = require("path");
const fs = require("fs-extra");

// Cesium
let cesiumDir = path.dirname(require.resolve("cesium"));
let cesiumStatic = path.join(__dirname, "static/cesium");
let cesiumAssets = [
  {
    from: path.join(cesiumDir, "Build/Cesium/Workers"),
    to: path.join(cesiumStatic, "Workers")
  },
  {
    from: path.join(cesiumDir, "Source/Assets"),
    to: path.join(cesiumStatic, "Assets")
  },
  {
    from: path.join(cesiumDir, "Source/Widgets"),
    to: path.join(cesiumStatic, "Widgets")
  },
  {
    from: path.join(cesiumDir, "Build/Cesium/Cesium.js"),
    to: path.join(cesiumStatic, "Cesium.js")
  }
];
function copyStatic() {
  fs.emptyDirSync(cesiumStatic);
  cesiumAssets.forEach(({ from, to }) => {
    try {
      fs.copySync(from, to);
    } catch (e) {
      console.error(e);
      console.log("failed copying cesium source files");
    }
  });
}
copyStatic();
