const path = require("path");
const fs = require("fs-extra");
const chokidar = require("chokidar");

module.exports = function(vasco) {
  let contentPath = path.resolve(vasco.getDir("build"), "./content/client");

  // copy utils
  fs.copySync(
    path.resolve(__dirname, "utils"),
    path.resolve(contentPath, "utils")
  );

  // locations
  let placesSavesPath = path.resolve(
    vasco.getDir("build"),
    "places/saves.json"
  );
  function writeLocations() {
    let saves = fs.readJsonSync(placesSavesPath);
    let locations = saves.locations;
    fs.outputJsonSync(path.resolve(contentPath, "locations.json"), locations);
  }
  chokidar.watch([placesSavesPath]).on("all", (event, path) => {
    writeLocations();
  });

  // algorithms
  let algorithms = [];
  vasco.algorithms.array().forEach(algo => {
    let clientSrc = path.resolve(algo.$path, "./client");

    if (fs.pathExistsSync(clientSrc)) {
      algorithms.push(algo.$name);

      let destinationPath = path.resolve(contentPath, "algorithms", algo.$name);

      // watch
      chokidar
        .watch([clientSrc], {
          ignoreInitial: true
        })
        .on("all", (event, path) => {
          fs.copySync(clientSrc, destinationPath);
        });

      fs.copySync(clientSrc, destinationPath);
    }
  });
  // generate algorithms' index.js
  let index = "";
  algorithms.forEach(name => {
    index += `import ${name} from "./${name}"\n`;
  });
  index += `const algorithms = { ${algorithms.join(", ")} }\n`;
  index += `export default algorithms\n`;
  fs.outputFileSync(path.resolve(contentPath, "algorithms/index.js"), index);
};
