import _ from "lodash";

let defaultPathOpts = {
  polyline: {
    color: new Cesium.Color(1, 1, 1, 1),
    width: 3
  }
};
let defaultNodeOpts = {
  colors: {
    unprocessed: Cesium.Color.CRIMSON,
    forbidden: Cesium.Color.ORANGERED,
    navigable: Cesium.Color.LIGHTBLUE,
    special: Cesium.Color.LIGHTGREEN
  },
  point: {
    show: true,
    scaleByDistance: new Cesium.NearFarScalar(10e3, 1, 10e6, 0.5)
  },
  label: {
    distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 2e6),
    scale: 0.5,
    show: true
  }
};
let defaultEdgesOpts = {
  polyline: {
    material: new Cesium.Color(0, 1, 0, 0.5),
    distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 2e6)
  },
  label: {
    distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 2e6),
    scale: 0.4,
    showBackground: true
  }
};

function setView(vm, options) {
  let { from, to } = options;
  vm.viewer.camera.setView({
    destination: new Cesium.Cartesian3.fromDegrees(
      (from.lon + to.lon) / 2,
      (from.lat + to.lat) / 2,
      8000000
    )
  });
}

function cleanup(vm) {
  vm.viewer.entities.removeAll();
}

function displayPath(vm, results, options = {}) {
  let opts = {};
  _.merge(opts, defaultPathOpts, options);
  let degreesArray = results.path.points
    .map(point => {
      let { lat, lon } = point;
      return [lon, lat];
    })
    .flat();
  vm.viewer.entities.add({
    polyline: {
      positions: new Cesium.Cartesian3.fromDegreesArray(degreesArray),
      ...opts.polyline
    }
  });
}

function displayNodes(vm, results, options = {}) {
  let opts = {};
  _.merge(opts, defaultNodeOpts, options);

  let nodes = results.graph.nodes;
  nodes.forEach(node => {
    let text = `${node.location.x}/${node.location.y} - ${
      node.cost != undefined ? `${node.cost.toFixed()} km` : "x"
    }`;

    // color
    let color = node.navigable ? opts.colors.navigable : opts.colors.forbidden;
    if (node.special) {
      color = opts.colors.special;
    }

    let entityOptions = {
      position: new Cesium.Cartesian3.fromDegrees(
        node.location.lon,
        node.location.lat
      ),
      label: new Cesium.LabelGraphics({
        text,
        showBackground: true,
        pixelOffset: new Cesium.Cartesian2(0, -10),
        ...opts.label
      }),
      point: new Cesium.PointGraphics({
        color,
        pixelSize: 10,
        ...opts.point
      })
    };
    vm.viewer.entities.add(entityOptions);
  });
}

function displayEdges(vm, results, options = {}) {
  let opts = {};
  _.merge(opts, defaultEdgesOpts, options);

  let nodes = {};
  results.graph.nodes.forEach(node => (nodes[node.id] = node));
  results.graph.nodes.forEach(node => {
    if (node.score != undefined) {
      node.edges.forEach(edge => {
        let from = node.location;
        let to = nodes[edge.to].location;
        let arr = [from, to].map(({ lat, lon }) => [lon, lat]).flat();
        let t = 1 / 5;
        vm.viewer.entities.add({
          position: new Cesium.Cartesian3.fromDegrees(
            t * from.lon + (1 - t) * to.lon,
            t * from.lat + (1 - t) * to.lat
          ),
          label: new Cesium.LabelGraphics({
            text: edge.score.toFixed(4),
            ...opts.label
          })
        });
        vm.viewer.entities.add({
          polyline: {
            positions: new Cesium.Cartesian3.fromDegreesArray(arr),
            ...opts.polyline
          }
        });
      });
    }
  });
}

export { setView, cleanup, displayPath, displayNodes, displayEdges };
