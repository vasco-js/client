const { Nuxt, Builder } = require("nuxt");

let express = require("express");

class VascoClient {
  constructor(vasco) {
    this.vasco = vasco;
    this.config = vasco.config;
  }
  init() {
    this.nuxt = new Nuxt(require("./configure-nuxt.js")(this.vasco));
    this.builder = new Builder(this.nuxt);

    this.devServer = express();
    this.devServer.use(this.render);
  }
  start() {
    this.builder.build();
  }
  get render() {
    return this.nuxt.render;
  }
}

module.exports = VascoClient;
