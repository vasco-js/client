const path = require("path");
const _ = require("lodash");
const webpack = require("webpack");

module.exports = function(vasco) {
  require("./copy-static");

  let VASCO_SERVER_URL = vasco.server.url;
  let dev = vasco.isDev;
  let client = vasco.config.get("client");
  let buildDir = vasco.getDir("build");

  let config = {
    dev,
    mode: "spa",
    buildDir: path.join(buildDir, ".client"),
    vue: {
      config: {
        productionTip: false
      }
    },
    modules: ["@nuxtjs/axios", ["~modules/nuxt-vasco", vasco]],
    axios: {
      baseURL: dev ? undefined : VASCO_SERVER_URL
    },
    head: {
      titleTemplate: "Vasco",
      script: [{ src: "/cesium/Cesium.js" }]
    },
    rootDir: __dirname,
    plugins: ["~/plugins/vue-vasco"],
    // vuetify
    buildModules: ["@nuxtjs/vuetify"],
    vuetify: {
      optionsPath: "./vuetify.options.js",
      defaultAssets: false
    },
    build: {
      quiet: !client.debug,
      plugins: [
        new webpack.DefinePlugin({
          "process.env.UI_VERSION": JSON.stringify(
            require("./package.json").version
          ),
          "process.env.VASCO_SERVER_URL": JSON.stringify(VASCO_SERVER_URL),
          "process.env.VASCO_UUID": JSON.stringify(vasco.id),
          // Define relative base path in cesium for loading assets
          CESIUM_BASE_URL: JSON.stringify("cesium")
        })
      ],
      extend(wpConfig, { isDev, isClient }) {
        _.assignIn(wpConfig, {
          externals: {
            Cesium: "Cesium"
          }
        });

        // fix vue module resolution when messing with linked packages at vasco dev time
        wpConfig.resolve.modules.unshift(
          path.resolve(vasco.getDir("root"), "node_modules")
        );

        wpConfig.resolve.alias["#content"] = path.resolve(
          vasco.getDir("build"),
          "./content/client"
        );
        // Sets webpack's mode to development if `isDev` is true.
        if (isDev) wpConfig.mode = "development";
      }
    }
  };
  return config;
};
