import "@fortawesome/fontawesome-free/css/all.css";

import colors from "vuetify/lib/util/colors";

export default {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: colors.blue.base,
        accent: colors.amber.base
      }
    }
  },
  icons: {
    iconfont: "fa"
  }
};
