import Vue from "vue";
import VueVasco from "./plugin";

Vue.config.productionTip = false;

Vue.use(VueVasco);
