import path from "path";

import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";

import io from "socket.io-client";

import Algorithms from "#content/algorithms";

let socket = io();

let VueVasco = {
  install(Vue) {
    let algorithms = [];

    Object.keys(Algorithms).forEach(name => {
      let algorithm = Algorithms[name];
      algorithm.name = name;
      algorithms.push(algorithm);
    });

    Vue.prototype.$vasco = {
      socket,
      algorithms
    };
    Vue.mixin({
      created() {
        if (this.$options.name == "VascoMap") {
          let map = this;
          let listeners = algorithms.forEach(({ name, events }) => {
            Object.entries(events || {}).forEach(([ev, handler]) => {
              map.$vasco.socket.on(`${name}:${ev}`, handler.bind(map));
            });
          });
        }
      }
    });

    // register components
    let requireComponent = require.context("./components", true, /\.vue$/);

    requireComponent.keys().forEach(fileName => {
      const componentConfig = requireComponent(fileName);

      const componentName = upperFirst(
        camelCase(fileName.replace(/^\.\//, "").replace(/\.\w+$/, ""))
      );

      Vue.component(componentName, componentConfig.default || componentConfig);
    });
  }
};

export default VueVasco;
